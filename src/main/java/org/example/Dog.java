package org.example;

public class Dog {
    int age;
    public String speak (){
        return "Bark bark...";
    }
    public void setAge(int age) throws InvalidAgeException{

        if(age < 0 || age > 20){
            throw new InvalidAgeException("Invalid age entered");
        }
        this.age = age;
    }
    public int getAge(){
        return age;
    }
}
