import org.example.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DogTests {
    Dog dog;
    @BeforeEach
    void createDog(){
        dog = new Dog();
    }

    @Test
    void testDogSpeak(){
        String expected = "Bark bark...";
        String actual = dog.speak();

        assertEquals(expected, actual);
    }
    @Test
    void testDogSetGetAge() throws InvalidAgeException {
        int expected = 4;
        dog.setAge(4);
        int actual = dog.getAge();

        assertEquals(expected, actual);
    }

    @Test
    void testSetAgeThrowsException(){
    InvalidAgeException e = assertThrows(InvalidAgeException.class, ()-> dog.setAge(100));
        assertEquals("Invalid age entered", e.getMessage());
    }
}
